package metodos.credito.banco.proway;

public class CreditoPessoalController {
	
	CreditoPessoal creditoPessoal;

	public CreditoPessoalController(CreditoPessoal creditoPessoal) {
		this.creditoPessoal = creditoPessoal;
	}
	
	
	/**
	 * Abre solicita��o de cr�dito pessoal e retorna o valor da parcela
	 * 
	 * @return valorDaParcela
	 */
	public Double solicitarCreditoPessoal() {
		double valorParcela = this.calcularValorParcela();
		this.creditoPessoal.setValorParcela(valorParcela);
		
		return this.creditoPessoal.getValorParcela();
	}
	
	/**
	 * Calcula o valor da parcela.
	 * 
	 * Retorna o valor da parcela a partir do valor desejado, quantidade de parcelas
	 * e taxa de juros informadas
	 *
	 * @return valorDaParcela
	 */
	private double calcularValorParcela () {
		
		double valorParcelas;
		double jurosPorParcela;
		
		// Considerando @param taxaJuros ao m�s e parcelas fixas
        // https://mundoeducacao.uol.com.br/matematica/tabela-price.htm
		jurosPorParcela = ((Math.pow((this.creditoPessoal.getTaxaJuros()/100)+1, 
				this.creditoPessoal.getQuantidadeParcelas())*
				(this.creditoPessoal.getTaxaJuros()/100))
				/(Math.pow((this.creditoPessoal.getTaxaJuros()/100)+1, 
						this.creditoPessoal.getQuantidadeParcelas())-1));
		
		valorParcelas = this.creditoPessoal.getValorDesejado() * jurosPorParcela;
		
		valorParcelas = Math.round(valorParcelas*100)/100.0d;
		
		return valorParcelas;
	}
}

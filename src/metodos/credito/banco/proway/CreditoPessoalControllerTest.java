package metodos.credito.banco.proway;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

class CreditoPessoalControllerTest {
	
	@Test
	void creditoPessoalControllerTest() {
		CreditoPessoal credito = new CreditoPessoal(25000.0, 10, 7.5);
		CreditoPessoalController solicitacao = new CreditoPessoalController(credito);
		double expected = 3642.15;
		double result = solicitacao.solicitarCreditoPessoal();
		
		Assert.assertEquals(expected, result, 0.00001);
	}

}

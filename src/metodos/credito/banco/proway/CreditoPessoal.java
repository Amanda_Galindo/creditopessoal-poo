package metodos.credito.banco.proway;

public class CreditoPessoal {
	private double valorDesejado;
	private int quantidadeParcelas;
	private double valorParcela;
	private double taxaJuros;
	
	public CreditoPessoal(double valorDesejado, int quantidadeParcelas, double taxaJuros) {
		this.setValorDesejado(valorDesejado);
		this.setQuantidadeParcelas(quantidadeParcelas);
		this.setTaxaJuros(taxaJuros);
	}

	public double getValorDesejado() {
		return valorDesejado;
	}

	public void setValorDesejado(double valorDesejado) {
		this.valorDesejado = valorDesejado;
	}

	public int getQuantidadeParcelas() {
		return quantidadeParcelas;
	}

	public void setQuantidadeParcelas(int quantidadeParcelas) {
		this.quantidadeParcelas = quantidadeParcelas;
	}

	public double getValorParcela() {
		return valorParcela;
	}

	public void setValorParcela(double valorParcela) {
		this.valorParcela = valorParcela;
	}

	public double getTaxaJuros() {
		return taxaJuros;
	}

	public void setTaxaJuros(double taxaJuros) {
		this.taxaJuros = taxaJuros;
	}
}

